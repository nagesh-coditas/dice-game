'use strict';

const player0El = document.querySelector('.player--0');
const player1El = document.querySelector('.player--1');
const score0El = document.querySelector('#score--0');
const score1El = document.getElementById('score--1');
const current0El = document.getElementById('current--0');
const current1El = document.getElementById('current--1');

const diceEl = document.querySelector('.dice');
const btnNew = document.querySelector('.btn--new');
const btnRoll = document.querySelector('.btn--roll');
const btnHold = document.querySelector('.btn--hold');


const inIt = function(){

    current0El.textContent = 0;
    current1El.textContent = 0;
    score0El.textContent = 0;
    score1El.textContent = 0;

    currentScore = 0;
    
    finalScore = [0,0];
    playing = true;
    document.querySelector('.dice').classList.add('hidden')
    // document.getElementById(`current--${activePlayer}`).classList.remove('player--active','player--winner');
    document.querySelector('.player--0').classList.remove('player--winner','player--active')
    document.querySelector('.player--1').classList.remove('player--active','player--winner')
    document.querySelector('.player--0').classList.add('player--active')
    // document.querySelector('.player--0').classList.remove('player--active','player--winner');
    activePlayer = 0;
}


//Rolling the dice

    diceEl.classList.add('hidden')
    current0El.textContent = 0;
    current1El.textContent = 0;
    score0El.textContent = 0;
    score1El.textContent = 0;
 
    let currentScore = 0;
    let activePlayer = 0;
    let finalScore = [0,0];
    let playing = true;

    const switchPlayer = function(){
        
        player0El.classList.toggle('player--active')
        player1El.classList.toggle('player--active')
        
        currentScore = 0;

        current0El.textContent = 0;
        current1El.textContent = 0;

            if (activePlayer === 1){

                    activePlayer = 0;
                    currentScore += dice
                    document.getElementById(`current--${activePlayer}`).textContent = currentScore

            }else
            {
                    activePlayer = 1;
                    currentScore += dice
                    document.getElementById(`current--${activePlayer}`).textContent = currentScore
            }

        
    }

btnRoll.addEventListener('click',function(){

    if (playing)
    {
        const dice = Math.trunc(Math.random()*6+1)
    console.log(dice)

    //Display Dice Element
    diceEl.classList.remove('hidden');
    diceEl.src = `dice-${dice}.png`;

    if(dice !== 1)
    {
    
    
    currentScore += dice;
    // console.log(dice)

    document.getElementById(`current--${activePlayer}`).textContent = currentScore
   
    }
    else{

        switchPlayer();

    }

    }
    
})

  //button to hold score
  btnHold.addEventListener('click', function(){
    
    //check active player and add final score
    if(playing){

        finalScore[activePlayer] += currentScore 
        document.getElementById(`score--${activePlayer}`).textContent = finalScore[activePlayer]
  
            //check the winner 
            if (finalScore[activePlayer] >= 20 )
            {
                playing = false;
                document.querySelector('.dice').classList.add('hidden')
                document.querySelector(`.player--${activePlayer}`).classList.add('player--winner')
                document.querySelector(`.player--${activePlayer}`).classList.remove('player--active')
            }

   switchPlayer();

    }

  })

    //restart the game
    btnNew.addEventListener('click',function(){
    // console.log("new game")
    inIt();


  })